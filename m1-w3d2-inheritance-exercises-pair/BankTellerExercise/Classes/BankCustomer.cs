﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankTellerExercise.Classes
{
    public class BankCustomer : BankAccount

    {
        private List<BankAccount> _allAccounts = new List<BankAccount>();

        //properties
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public BankAccount[] Accounts
        {
            get
            {
                return _allAccounts.ToArray();
            }
        }

        



        //methods
        public void AddAccount(BankAccount newAccount)
        {
            _allAccounts.Add(newAccount);
        }

        //public override string ToString()
        //{
        //    //tring aaa = "";
        //    foreach (BankAccount acount in Accounts)
        //    {
                
        //    }

        //    return " ";
        //}



        public BankCustomer(string name, string address, string phoneNumber)
        {
            Name = name;
            Address = address;
            PhoneNumber = phoneNumber;

        }

























    }





}
