﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankTellerExercise.Classes
{
    public class Savings : BankAccount
    {
        public override decimal Withdraw(decimal amountToWithdraw)
        {
            if (Balance-amountToWithdraw<0)
            {
                amountToWithdraw = 0;
            }
            else if (Balance<150)
            {
                amountToWithdraw += 2;
            }

            return amountToWithdraw;
        }
    }
}
