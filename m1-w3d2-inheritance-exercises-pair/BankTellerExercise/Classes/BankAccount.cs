﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankTellerExercise.Classes
{
    public class BankAccount
    {
        

        //properties
        public string AccountNumber { get; set; }
        public decimal Balance { get; protected set; }
        
        public decimal TotalBalance
        {
            get
            {
                
                
                return 0;
            }
        }

        //constuctors
        public BankAccount()
        {
            Balance = 0;
        }

      
        //methods
        public decimal Deposit(decimal amountToDeposit)
        {
            Balance += amountToDeposit;
            return Balance;
        }

        

        public virtual decimal Withdraw(decimal amountToWithdraw)
        {

            Balance -= amountToWithdraw;
            return Balance;
        }

        public void Transfer(BankAccount destinationAccount, decimal transferAmount)
        {
            Balance -= transferAmount;
            destinationAccount.Balance += transferAmount;
                
        }
    }
}
