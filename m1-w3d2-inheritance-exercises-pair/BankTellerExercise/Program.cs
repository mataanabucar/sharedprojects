﻿using BankTellerExercise.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankTellerExercise
{
    class Program
    {
        static void Main(string[] args)
        {

            BankCustomer custOne = new BankCustomer("Mataan", "2522 cypress way", "6142708183");

            Checking matChecking = new Checking();
            Savings matSavings = new Savings();
            matChecking.Deposit(20000);
            matSavings.Deposit(20000);
            custOne.AddAccount(matChecking);
            custOne.AddAccount(matSavings);



            Console.WriteLine(custOne.Balance);

            Console.ReadKey();


        }
    }
}
