﻿using System;
using BankTellerExercise.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BankTellerExerciseTests
{
    [TestClass]
    public class UnitTests
    {

        BankAccount exercise = new BankAccount();


        [TestMethod]
        public void depositTest()
        {
            Assert.AreEqual(10, exercise.Deposit(10), "Input: deposit (10)");

        }
        [TestMethod]
        public void withdrawTest()
        {
            Assert.AreEqual(-10, exercise.Withdraw(10), "Input: withdraw (10)");
        }
        //[TestMethod]
        //public void transferTest()
        //{
            
        //}

        //Checking exercise1 = new Checking();
        //BankAccount mataanAccount = new BankAccount();
        //mataanAccount.SetBalance(-100);

        [TestMethod]
        public void checkingWithdraw()
        {
            Checking mataanAccount = new Checking();
            mataanAccount.Deposit(20);
            Assert.AreEqual(50, mataanAccount.Withdraw(40));
            
        }


        [TestMethod]
        public void checkingSaving()
        {
            Savings mataanAccount = new Savings();
            mataanAccount.Deposit(200);
            Assert.AreEqual(40, mataanAccount.Withdraw(40));
            
            Assert.AreEqual(60, mataanAccount.Withdraw(60));
        }

        [TestMethod]
        public void checkingSavingFee()
        {
            Savings mataanAccount = new Savings();
            mataanAccount.Deposit(140);
            Assert.AreEqual(42, mataanAccount.Withdraw(40));

        }


        }
    }
        
    

