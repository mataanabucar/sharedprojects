﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayroll
{
    public class SalaryWorker : IWorker
    {
        public double AnnualSalary { get; }

        public string FirstName { get; }

        public string LastName { get; }

        public double CalculateWeeklyPay(int hoursWorked)
        {
            double pay = (AnnualSalary / 52);
            pay = Math.Round(pay, 2);
            return pay;
        }



        public SalaryWorker(double annualSalary, string firstName, string lastName)
        {
            AnnualSalary = annualSalary;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}
