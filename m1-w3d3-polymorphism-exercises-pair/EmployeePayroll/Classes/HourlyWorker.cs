﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeePayroll
{
    public class HourlyWorker : IWorker
    {
        public string FirstName { get; }

        public string LastName { get; }

        public double HourlyRate { get; }

        public double CalculateWeeklyPay(int hoursWorked)
        {
            double pay = HourlyRate * hoursWorked;
            double overtime = (hoursWorked - 40);
            if (overtime>0)
            {
                pay = pay + (HourlyRate * overtime * .5);
            }

            return pay;
        }


        public HourlyWorker(double hourlyRate, string firstName, string lastName)
        {
            HourlyRate = hourlyRate;
            FirstName = firstName;
            LastName = lastName;

        }










    }
}
