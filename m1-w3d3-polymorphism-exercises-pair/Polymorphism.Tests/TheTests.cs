﻿using EmployeePayroll;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Polymorphism.Tests
{
    [TestClass]
    public class TheTests
    {

        

        [TestMethod]
        public void salaryWorker()
        {
            SalaryWorker mat = new SalaryWorker(52000, "Mataan", "Abucar");
            Assert.AreEqual(1000, mat.CalculateWeeklyPay(56));
            SalaryWorker drew = new SalaryWorker(40000, "Drew", "Keriazes");
            Assert.AreEqual(769.23, drew.CalculateWeeklyPay(56));

        }


        [TestMethod]
        public void hourlyWorker()
        {
            HourlyWorker mat = new HourlyWorker(12, "Mataan", "Abucar");
            Assert.AreEqual(480, mat.CalculateWeeklyPay(40));
            HourlyWorker drew = new HourlyWorker(12, "Drew", "Keriazes");
            Assert.AreEqual(750, drew.CalculateWeeklyPay(55));

        }

        [TestMethod]
        public void volunWorker()
        {
            VolunteerWorker mat = new VolunteerWorker("mataan", "abucar");
            Assert.AreEqual(0, mat.CalculateWeeklyPay(56));



        }



    }
}
