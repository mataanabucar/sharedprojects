﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mataan_Test_Area
{
    class Program
    {
        static void Main(string[] args)
        {

            Deck deck = new Deck();

            string card = "";
            for (int i = 0; i < 52; i++)
            {
                Card newCard = deck.Cards[i];

                card = $"{newCard.Color} {newCard.Value} of {newCard.Suit}";
                Console.WriteLine(card);
            }

            Console.ReadKey();

        }
    }
}
