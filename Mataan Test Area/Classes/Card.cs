﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mataan_Test_Area
{
    class Card
    {

        public const int Spades = 1;
        public const int Clubs = 2;
        public const int Hearts = 3;
        public const int Diamonds = 4;


        public string Suit { get; set; }

        public string Value { get; set; }

        public string Color
        {
            get
            {
                if (Suit == "Hearts" || Suit == "Diamonds")
                {
                    return "Red";
                }
                else
                {
                    return "Black";
                }
            }
        }

        public Card(int cardSuit, int cardNumber)
        {
            Suit = setSuit[cardSuit];
            Value = setNumber[cardNumber];
        }



        private static Dictionary<int, string> setNumber = new Dictionary<int, string>()
        {
            {1, "Ace" },
            {2, "Two" },
            {3, "Three" },
            {4, "Four" },
            {5, "Five" },
            {6, "Six" },
            {7, "Seven" },
            {8, "Eight" },
            {9, "Nine" },
            {10, "Ten" },
            {11, "Jack" },
            {12, "Queen" },
            {13, "King" }
        };


        private static Dictionary<int, string> setSuit = new Dictionary<int, string>()
        {
            {1, "Spades" },
            {2,"Clubs" },
            {3,"Hearts" },
            {4,"Diamonds" }
        };
          


          
        
        
            





    }
}
