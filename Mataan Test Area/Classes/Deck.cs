﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mataan_Test_Area
{
    class Deck
    {
        public List<Card> Cards = new List<Card>();

        public Deck()
        {
            for (int i = 1; i < 14; i++)
            {
                Card c = new Card(Card.Spades, i);
                Cards.Add(c);
            }
            for (int i = 1; i < 14; i++)
            {
                Card c = new Card(Card.Clubs, i);
                Cards.Add(c);
            }
            for (int i = 1; i < 14; i++)
            {
                Card c = new Card(Card.Diamonds, i);
                Cards.Add(c);
            }
            for (int i = 1; i < 14; i++)
            {
                Card c = new Card(Card.Hearts, i);
                Cards.Add(c);
            }

        }
        
        



    }
}
